# rss_campaign #

This module allows a campaign parameter to be added to every URL in an RSS feed.  This allows clicks on the URLs to be tracked by analytics services such as Google Analytics.